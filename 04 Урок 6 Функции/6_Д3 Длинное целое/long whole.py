n = input()
if "." in n:
    if "-" in n:
        print(int(float(n)) * (-1))
    else:
        print(int(float(n)))
else:
    if "-" in n:
        print(len(n) - 1)
    else:
        print(len(n))