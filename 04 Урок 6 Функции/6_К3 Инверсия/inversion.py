n = float(input())
if abs(n) < float("1e-9"):
    print("INFINITELY LARGE")
elif abs(n) > float("1e9"):
    print("INFINITELY SMALL")
else:
    print(1/n)
