fishes = dict()
fish_depth = input()
while fish_depth:
    fish_depth = fish_depth.split()
    if fish_depth[0] not in fishes:
        fishes[fish_depth[0]] = (int(fish_depth[1]), )
    else:
        fishes[fish_depth[0]] += (int(fish_depth[1]), )
    fish_depth = input()
for i in fishes:
    fish_tuple = (min(fishes[i]), max(fishes[i]))
    fishes[i] = fish_tuple
print(fishes)
