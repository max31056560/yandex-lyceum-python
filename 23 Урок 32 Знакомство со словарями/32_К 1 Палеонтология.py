mil = 1000000
tho = 1000
years_ago = input()
while years_ago:
    years_ago = int(years_ago) * tho
    if years_ago < 145 * mil:
        print('Cenozoic')
    if 300 * mil > years_ago >= 145 * mil:
        print('Mesozoic')
    if 635 * mil > years_ago >= 300 * mil:
        print('Paleozoic')
    if 2800 * mil > years_ago >= 635 * mil:
        print('Proterozoic')
    if 2800 * mil < years_ago:
        print('Archaea')
    years_ago = input()
    #  print(f'years_ago = {years_ago}')