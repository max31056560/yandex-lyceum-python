numbers = [int(x) for x in input().split()]
answer = list()
for i in range(len(numbers)):
    n_dict = {'digits': len(bin(numbers[i])[2:]), 'units': bin(numbers[i])[2:].count('1'),
              'zeros': bin(numbers[i])[2:].count('0')}
    answer.append(n_dict)
print(answer)
