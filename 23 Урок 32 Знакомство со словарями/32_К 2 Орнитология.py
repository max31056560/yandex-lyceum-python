bird = input()
birds = {}
while bird:
    bird = bird.split(': ')
    # print(bird[0], bird[1])
    if bird[0] not in birds:
        birds[bird[0]] = int(bird[1])
        # print(f'bird {bird[0]} is now in birds')
        # print(birds)
    else:
        birds[bird[0]] += int(bird[1])
        # print(birds)
    bird = input()
print(birds)