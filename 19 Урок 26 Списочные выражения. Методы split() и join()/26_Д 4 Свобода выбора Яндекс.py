divisor, n, s = input().split()
print(divisor[::-1].join([x for x in s.split(divisor) if len(set(x)) >= int(n)]))