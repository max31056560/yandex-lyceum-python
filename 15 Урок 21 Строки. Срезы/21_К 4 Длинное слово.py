line = input()
max_word_len = word_len = 0
max_word = word = ''
while line:
    if line[0] == ' ':
        line = line[1:len(line)]
    else:
        word_len = 0
        word = ''
        while line[0] != ' ':
            word_len += 1
            word += line[0]
            if word_len > max_word_len:
                max_word_len = word_len
                max_word = word
            line = line[1:len(line)]
            if not line:
                break
print(max_word)