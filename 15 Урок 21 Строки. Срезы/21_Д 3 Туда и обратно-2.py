line = input()
line_without_spaces = ''
for i in line:
    if i == ' ':
        continue
    line_without_spaces += i
print(line_without_spaces == line_without_spaces[::-1])
