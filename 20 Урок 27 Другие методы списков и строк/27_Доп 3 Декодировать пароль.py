pass_word = input()
pass_list = list(pass_word)
answer = list()
last_char_is_appended_with_percent = False
percent_count = 0
last_char = ''

while pass_word.startswith('%'):  # Отчистили от начинающихся % в строке
    pass_list = list(pass_word)
    del pass_list[0]
    pass_word = ''.join(pass_list)

print(pass_word)
print(pass_list)
# print(pass_list[0:2])

while len(pass_list) >= 3:
    if pass_list[0] == pass_list[1]:
        if '%' not in pass_list[0]:
            answer.append(pass_list[0])
            last_char = pass_list[0]
            del pass_list[0:2]
        if '%' in pass_list[0] and percent_count < 3:
            last_char = pass_list[0]
            percent_count += 2
            del pass_list[0:2]

    else:
        del pass_list[0:2]

print('answer = ', ''.join(answer))
