separator = input()
strings = input().upper().split()
for i in range(len(strings)):
    if len(strings[i]) > 1:
        word_list = list(strings[i])
        strings[i] = separator.join(word_list)
print(*strings)
