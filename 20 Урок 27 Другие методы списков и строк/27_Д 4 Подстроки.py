string = input()
substring_set = set()
for i in range(1, len(string) // 2):  #  i будет длиной подстроки, по которой будем искать повторение
    for j in range(len(string)):  # j будет начальный индекс подстроки, по которой будем искать повторение
        cur_substring = string[j:j + i]
        # print('cur_substring = ', cur_substring)
        if cur_substring not in substring_set:
            substring_set.add(cur_substring)

            #  print('cur_substring = ', cur_substring)
            if string.count(cur_substring) > 1:
                print(cur_substring + ': ' + str(string.count(cur_substring)))
