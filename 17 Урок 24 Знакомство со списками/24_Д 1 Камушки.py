stones = list()
stones_sum = 0
remained_stones = list()
removed_stones = list()
removes_stones_sum = 0
for i in range(int(input())):
    stones.append(int(input()))
    stones_sum += stones[i]

if stones_sum % 2 == 0:
    for i in range(len(stones)):
        if stones[i] % 2 != 0:
            removes_stones_sum += stones[i]
            removed_stones.append(stones[i])
        else:
            remained_stones.append(stones[i])
else:
    for i in range(len(stones)):
        if stones[i] % 2 == 0:
            removes_stones_sum += stones[i]
            removed_stones.append(stones[i])
        else:
            remained_stones.append(stones[i])

print(removes_stones_sum, remained_stones)
