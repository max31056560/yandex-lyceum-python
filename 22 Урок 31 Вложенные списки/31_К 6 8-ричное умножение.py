start = int(input())
end = int(input())
side = (end - start + 2)
table = [[0] * side for _ in range(side)]
table[0][0] = 0
row_i = int(input())
col_i = int(input())

for i in range(1, side):
    table[0][i] = start + i - 1
    table[i][0] = start + i - 1

# debug
# for row in range(side):
#     for col in range(side):
#         print(table[row][col], end='\t')
#     print()

for row in range(1, side):
    for col in range(1, side):
        table[row][col] = table[row][0] * table[0][col]
        # table[row][col] = int(oct(table[row][col])[2:])

for row in range(side):
    for col in range(side):
        if row == col == 0:
            print('', end='\t')
            continue
        print(oct(int(table[row][col]))[2:], end='\t')
    print()
print(oct(int(table[row_i + 1][col_i + 1]))[2:])
