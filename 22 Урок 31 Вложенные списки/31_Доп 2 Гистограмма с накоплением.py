symbols = '*+ox-<'
marks = list()
line = input()
histogramm = list()
max_col_sum = 0
col_sum = 0
l_col_sums = list()
while line:
    marks.append([int(x) for x in line.split(', ')])
    line = input()

for col in range(len(marks[0])):
    for row in range(len(marks)):
        col_sum += marks[row][col]
    l_col_sums.append(col_sum)
    if col_sum > max_col_sum:
        max_col_sum = col_sum
    col_sum = 0
# print(f'max_col_sum = {max_col_sum}')
# print(f'l_col_sum = {l_col_sums}')
# print('marks:', *marks, sep='\n')
histogramm = [([0] * len(marks[0])) for i in range(max_col_sum)]
#  histogramm[5][1] = 5
for col in range(len(histogramm[0])):
    index = len(histogramm) - 1
    for i_marks in range(len(marks)):
        for symbol_repeat in range(marks[i_marks][col]):
            histogramm[index][col] = symbols[i_marks % 6] * 2
            index -= 1
            # print(f'col = {col}, index = {index}')

# print('histogram:', *histogramm, sep='\n')
for i in range(len(histogramm)):
    for j in range(len(histogramm[0])):
        if histogramm[i][j] == 0:
            print(' ' * 2, end=' ')
        else:
            print(histogramm[i][j], end=' ')
    print()
