n = int(input())
m = int(input())
l_predv = []
rusalki = []
answer = []
for i in range(n):
    for j in range(m):
        l_predv.append(input())
    rusalki.append(l_predv[:])
    l_predv.clear()
# print(*rusalki, sep='\n')
for i in range(n):
    for j in range(m):
        # print(f'i = {i}, j = {j}')
        if i == n - 1 and j != m - 1:
            # проверка правой
            if not (len(rusalki[i][j]) == len(rusalki[i][j + 1]) or rusalki[i][j][0] == rusalki[i][j + 1][0]):
                # print(f'русалки ({i}, {j} и ({i}, {j + 1}) не подошли друг другу (лево право)')
                answer.append((i, j))
                answer.append((i, j + 1))
        elif j == m - 1 and i != n - 1:
            # проверка нижней
            if not (len(rusalki[i][j]) != len(rusalki[i + 1][j]) and rusalki[i][j][0] != rusalki[i + 1][j][0]):
                # print(f'русалки({i}, {j} и ({i + 1}, {j}) не подошли друг другу (верх-них)')
                answer.append((i, j))
                answer.append((i + 1, j))
        elif i == n - 1 and j == m - 1:
            continue
        else:
            # проверка правой
            if not (len(rusalki[i][j]) == len(rusalki[i][j + 1]) or rusalki[i][j][0] == rusalki[i][j + 1][0]):
                # print(f'русалки ({i}, {j} и ({i}, {j + 1}) не подошли друг другу (лево право)')
                answer.append((i, j))
                answer.append((i, j + 1))
            # проверка нижней
            if not(len(rusalki[i][j]) != len(rusalki[i + 1][j]) and rusalki[i][j][0] != rusalki[i + 1][j][0]):
                # print(f'русалки({i}, {j} и ({i + 1}, {j}) не подошли друг другу (верх-них)')
                answer.append((i, j))
                answer.append((i + 1, j))
answer = list(set(answer))
answer.sort()
print(*answer, sep='\n')
