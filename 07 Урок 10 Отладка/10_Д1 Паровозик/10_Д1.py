h = int(input())
m = int(input())
n = int(input())

stop_time = 0
real_time = h * 60 + m
time_limit = 12 * 60

if real_time >= time_limit:
    print("Не останавливаемся!")
else:
    while real_time < time_limit:
        stop_time += 1
        real_time = real_time + stop_time * n
    print(stop_time, "минут")
