number = int(input())
step = 0
while number != 6174:
    n_1, n_2, n_3, n_4 = number // 1000, number // 100 % 10, number % 100 // 10, number % 10
    if n_1 > n_2:
        n_1, n_2 = n_2, n_1
    if n_2 > n_3:
        n_2, n_3 = n_3, n_2
    if n_3 > n_4:
        n_3, n_4 = n_4, n_3
    if n_1 > n_2:
        n_1, n_2 = n_2, n_1
    if n_2 > n_3:
        n_2, n_3 = n_3, n_2
    if n_1 > n_2:
        n_1, n_2 = n_2, n_1

    larger = n_4 * 1000 + n_3 * 100 + n_2 * 10 + n_1
    smaller = n_1 * 1000 + n_2 * 100 + n_3 * 10 + n_4
    number = larger - smaller
    step += 1
    print(larger, smaller, number)
print(step)