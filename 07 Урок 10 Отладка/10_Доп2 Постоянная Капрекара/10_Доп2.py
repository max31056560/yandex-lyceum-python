number = int(input())
cycle_count = 0
while number != 6174:
    digit_max = max(number % 10, number // 10 % 10, number // 100 % 10, number // 1000 % 10)
    digit_min = min(number % 10, number // 10 % 10, number // 100 % 10, number // 1000 % 10)
    n2_s = ""
    for i in range(4):
        cur_digit = number // 10 ** i % 10
        if cur_digit != digit_max and cur_digit != digit_min:
            n2_s = n2_s + str(cur_digit)
    if n2_s == "":
        n2 = digit_min * 10 + digit_max
    else:
        n2 = int(n2_s)
    if n2 % 10 > n2 // 10:
        digit_max_2 = n2 % 10
        digit_min_2 = n2 // 10
    else:
        digit_max_2 = n2 // 10
        digit_min_2 = n2 % 10
    max_number = digit_max * 1000  + digit_max_2 * 100 + digit_min * 10 + digit_min_2
    min_number = digit_min * 1000  + digit_min_2 * 100 + digit_max_2 * 10 + digit_max
    if digit_min == 0:
        min_number = min_number // 10
    cycle_count += 1
    number = max_number - min_number
    print( max_number, min_number, number)
    if cycle_count > 500:
        print("Похоже что-то идет не так")
        print("Слишком много циклов, я так быстро не могу")
        break
print(cycle_count)