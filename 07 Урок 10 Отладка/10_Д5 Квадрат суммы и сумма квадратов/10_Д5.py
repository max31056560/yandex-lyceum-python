n = int(input())
n_sum = 0
n_square_sum = 0
for i in range(n + 1):
    n_square_sum = n_square_sum + i ** 2
    n_sum += i
print(n_sum ** 2 - n_square_sum)