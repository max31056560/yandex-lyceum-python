desk_num = int(input())
s = input()
desk_count = 0
while s != 'КОНЕЦ':
    if 'доска' in s or 'дощечка' in s:
        desk_count += 1
        print('Прибили', desk_count, 'дощечку.')
        desk_num -= 1
    if desk_num == 0:
        print('ГОТОВО')
        break
    s = input()
if desk_num > 0:
    print('МАЛОВАТО')