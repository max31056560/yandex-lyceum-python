s1, s2, s3 = input(), input(), input()
print(s1 * (s1 < s2 and s1 < s3) +
      s2 * (s2 < s1 and s2 < s3) +
      s3 * (s3 < s1 and s3 < s2)
      )