flower_1 = input()
flower_2 = input()
flower_3 = input()
present_flower_1 = False
present_flower_2 = False
present_flower_3 = False
answer = ""
bouquet = input()
if flower_1 in bouquet:
    present_flower_1 = True
if flower_2 in bouquet:
    present_flower_2 = True
if flower_3 in bouquet:
    present_flower_3 = True
    
if present_flower_1 and present_flower_2 and present_flower_3:
    answer = flower_1 + ", " + flower_2 + ", " + flower_3
elif present_flower_1 and present_flower_2:
    answer = flower_1 + ", " + flower_2
elif present_flower_2 and present_flower_3:
    answer = flower_2 + ", " + flower_3
elif present_flower_1 and present_flower_3:
    answer = flower_1 + ", " + flower_3
elif present_flower_1:
    answer = flower_1
elif present_flower_2:
    answer = flower_2
elif present_flower_3:
    answer = flower_3
if present_flower_1 or present_flower_2 or present_flower_3:
    print("В букете есть " + answer + ".")
else:
    print("Таких цветов в букете нет.")
