n = int(input())
count = 1
i = 2
while count < n:
    i += 1
    x = i
    while x % 2 == 0:
        x //= 2
    while x % 3 == 0:
        x //= 3
    while x % 5 == 0:
        x //= 5
    if x == 1:
        count += 1
print(i)