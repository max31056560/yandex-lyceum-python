s = int(input())
j_prev = 0
for i in range(1, s + 1):
    for j in range(1, s + 1):
        if i == j_prev:
            break
        if i * j == s:
            print(i, j)
        j_prev = j