string = input()
answer = int(string[0])
for i in range(1, len(string), 2):
    if string[i] == '+':
        answer += int(string[i + 1])
    if string[i] == '-':
        answer -= int(string[i + 1])
print(answer)