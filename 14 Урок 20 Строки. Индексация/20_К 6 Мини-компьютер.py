string = input()
string_set = set(string)
min_unicode = 10e10
max_unicode = 0
for char in string:
    if ord(char) > max_unicode:
        max_unicode = ord(char)
    if ord(char) < min_unicode:
        min_unicode = ord(char)
print(min_unicode, max_unicode, sep=', ')
if len(string_set) <= 32:
    print('ХВАТИТ')
else:
    print('НЕ ХВАТИТ')