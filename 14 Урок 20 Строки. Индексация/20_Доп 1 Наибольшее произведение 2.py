n = input()
sequence = int(input())
maximum = 0
for i in range(1000 - sequence + 1):
    sequence_mult = 1
    for j in range(sequence):
        sequence_mult *= int(n[i + j])
    if sequence_mult > maximum:
        maximum = sequence_mult
print(maximum)