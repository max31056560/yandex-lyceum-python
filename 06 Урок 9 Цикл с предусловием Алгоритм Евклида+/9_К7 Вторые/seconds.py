n = int(input())
max_new = max_old = 0
while abs(n) < 1000:
    if n > max_old and n > max_new:
        max_old, max_new = max_new, n
    if n > max_old and n < max_new:
        max_old = n
    n = int(input())
print(max_old)